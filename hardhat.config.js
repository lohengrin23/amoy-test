require("@nomiclabs/hardhat-waffle");

const endpointUrl = "https://polygon-amoy.g.alchemy.com/v2/NYG_Qj6cqHIReYKd4CU4Bdby9koSAg7M";
const privateKey = "865d06692fe69b2c1db4990ace806cbaf1fe80c9615178d67206a1f95719dac3";

module.exports = {
  solidity: "0.8.21",
  networks: {
    amoyPloygon: {
      url: endpointUrl,
      accounts: [privateKey],
    },
  },
};